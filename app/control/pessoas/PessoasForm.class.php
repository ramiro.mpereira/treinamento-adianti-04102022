<?php

use Adianti\Base\TStandardForm;
use Adianti\Control\TAction;
use Adianti\Core\AdiantiCoreTranslator;
use Adianti\Database\TTransaction;
use Adianti\Validator\TRequiredValidator;
use Adianti\Widget\Container\TVBox;
use Adianti\Widget\Dialog\TMessage;
use Adianti\Widget\Form\TEntry;
use Adianti\Widget\Form\TLabel;
use Adianti\Widget\Form\TRadioGroup;
use Adianti\Widget\Util\TXMLBreadCrumb;
use Adianti\Wrapper\BootstrapFormBuilder;

class PessoasForm extends TStandardForm
{
    protected $form; // form

    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct($param)
    {
        parent::__construct();

        // creates the form

        $this->form = new BootstrapFormBuilder('form_Pessoas');
        $this->form->setFormTitle('Pessoa');

        // defines the database
        parent::setDatabase('treinamento');

        // defines the active record
        parent::setActiveRecord('Pessoa');

        // create the form fields
        $id            = new TEntry('id');

        $nome    = new TEntry('nome');

        $cpf          =  new TEntry('cpf');
        $cpf->setMask('999.999.999-99',true);

        $sexo     = new TRadioGroup('sexo');
        $opcoesSexo = ["M" => 'Masculino', "F" => 'Feminino'];
        $sexo->addItems($opcoesSexo);

        $id->setEditable(false);


        // add the fields
        $this->form->addFields([new TLabel('ID')], [$id]);
        $this->form->addFields([new TLabel("Nome")], [$nome]);
        $this->form->addFields([new TLabel("CPF")], [$cpf]);
        $this->form->addFields([new TLabel("Sexo")], [$sexo]);

        $id->setSize('30%');
        $nome->setSize('70%');
        $cpf->setSize('70%');

        // validations
        $nome->addValidation("Nome", new TRequiredValidator);
        $sexo->addValidation("Sexo", new TRequiredValidator);

        // add form actions
        $btn = $this->form->addAction(_t('Save'), new TAction(array($this, 'onSave')), 'far:save');
        $btn->class = 'btn btn-sm btn-primary';

        $this->form->addActionLink(_t('Clear'), new TAction(array($this, 'onEdit')), 'fa:eraser red');
        $this->form->addActionLink(_t('Back'), new TAction(array('PessoasList', 'onReload')), 'far:arrow-alt-circle-left blue');

        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add(new TXMLBreadCrumb('menu.xml', 'PessoasList'));
        $container->add($this->form);


        // add the container to the page
        parent::add($container);
    }

    public function onEdit($param)
    {
        try {
            if (isset($param['key'])) {
                $key = $param['key'];

                TTransaction::open($this->database);
                $class = $this->activeRecord;
                $pessoa = new $class($key);

                $this->form->setData($pessoa);

                TTransaction::close();

                return $pessoa;
            } else {
                $this->form->clear();
            }
        } catch (Exception $e) // in case of exception
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }


    public function onSave()
    {
        try {
            TTransaction::open($this->database);

            $dadosDoFormulario = $this->form->getData();

            $pessoa = new Pessoa;
            $pessoa->id = $dadosDoFormulario->id;
            $pessoa->nome = $dadosDoFormulario->nome;
            $pessoa->cpf = $dadosDoFormulario->cpf;
            $pessoa->sexo = $dadosDoFormulario->sexo;

            $this->form->validate();
            $pessoa->store();
            $dadosDoFormulario->id = $pessoa->id;
            $this->form->setData($dadosDoFormulario);

            TTransaction::close();

            new TMessage('info', AdiantiCoreTranslator::translate('Record saved'));

            return $pessoa;
        } catch (Exception $e) // in case of exception
        {
            // get the form data
            $pessoa = $this->form->getData($this->activeRecord);
            $this->form->setData($pessoa);
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
}
