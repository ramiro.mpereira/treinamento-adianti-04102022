<?php

use Adianti\Database\TRecord;

class Pessoa extends TRecord
{
    const TABLENAME  = 'pessoas';
    const PRIMARYKEY = 'id';
    const IDPOLICY   = 'serial'; // {max, serial}

    public function __construct($id = NULL)
    {
        parent::__construct($id);
        parent::addAttribute('nome');
        parent::addAttribute('cpf');
        parent::addAttribute('sexo');
    }
    
}
